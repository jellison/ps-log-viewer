﻿using System.Data;
using System.Data.SqlClient;

namespace logviewer.api.Domain.Data
{
    public interface IConnectionFactory
    {
        IDbConnection Create();
    }

    public class ConnectionFactory : IConnectionFactory
    {
        private readonly string _connectionString;

        public ConnectionFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IDbConnection Create()
        {
            return new SqlConnection(_connectionString);
        }
    }
}
