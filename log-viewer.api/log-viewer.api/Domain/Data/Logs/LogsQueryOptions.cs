﻿using System;
using System.Collections.Generic;

namespace logviewer.api.Domain.Data.Logs
{
    public class LogsQueryOptions
    {
        public string ApplicationName { get; set; }
        public DateTime RangeStart { get; set; }
        public DateTime RangeEnd { get; set; }
        public List<string> Severities { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
    }
}
