﻿using System.Collections.Generic;
using System.Linq;
using Dapper;

namespace logviewer.api.Domain.Data.Logs
{
    public interface ILogsRepository
    {
        List<Log> GetLogs(LogsQueryOptions options);
        List<Log> GetErrors(LogsQueryOptions options);
    }

    public class LogsRepository : ILogsRepository
    {
        private readonly IConnectionFactory _factory;

        public LogsRepository(IConnectionFactory factory)
        {
            _factory = factory;
        }

        public List<Log> GetErrors(LogsQueryOptions options)
        {
            return GetRecords(options, "ErrorMessage");
        }

        public List<Log> GetLogs(LogsQueryOptions options)
        {
            return GetRecords(options, "EventMessage");
        }

        private List<Log> GetRecords(LogsQueryOptions options, string tableName)
        {
            using (var con = _factory.Create())
            {
                var builder = new SqlBuilder();
                var template = builder.AddTemplate(
                    $@"SELECT * FROM {
                            tableName
                        } WITH (NOLOCK) /**where**/ ORDER BY CreatedUTC DESC OFFSET @Skip ROWS FETCH NEXT @Take ROWS ONLY",
                    new {options.Skip, options.Take}
                );

                builder.Where("CreatedUTC >= @RangeStart", new {options.RangeStart});
                builder.Where("CreatedUTC <= @RangeEnd", new {options.RangeEnd});

                // WHERE IN slows down the query considerably. If all severities are included, exclude the filter
                if (options.Severities?.Any() == true && options.Severities.Count < 5)
                {
                    builder.Where("Severity IN @Severities", new {options.Severities});
                }

                if (!string.IsNullOrEmpty(options.ApplicationName))
                {
                    builder.Where("ApplicationName LIKE @ApplicationName + '%'", new {options.ApplicationName});
                }

                return con.Query<Log>(template.RawSql, template.Parameters).ToList();
            }
        }
    }
}
