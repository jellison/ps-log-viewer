﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using logviewer.api.Domain.Data.Logs;
using Newtonsoft.Json;

namespace logviewer.api.Domain.Managers
{
    public class LogsManager
    {
        private const int PAGE_SIZE = 50;

        private static readonly ConcurrentDictionary<string, ConcurrentDictionary<int, int>> _queryPageCount;

        private readonly ILogsRepository _logsRepository;
        private readonly Models.ILogsBuilder _logsBuilder;

        static LogsManager()
        {
            _queryPageCount = new ConcurrentDictionary<string, ConcurrentDictionary<int, int>>();
        }

        public LogsManager(ILogsRepository logsRepository, Models.ILogsBuilder logsBuilder)
        {
            _logsRepository = logsRepository;
            _logsBuilder = logsBuilder;
        }

        public List<Models.Log> GetLogs(string applicationName, string query, int page, DateTime rangeStart, DateTime rangeEnd, List<string> severities)
        {
            List<Models.Log> models = null;
            var repoMethod = GetRepositoryMethod(severities);
            var options = new LogsQueryOptions(){
                Take = PAGE_SIZE,
                ApplicationName = applicationName,
                RangeStart = rangeStart,
                RangeEnd = rangeEnd,
                Severities = severities
            };
            
            if (!string.IsNullOrWhiteSpace(query))
            {
                var hash = Encoding.UTF8.GetString(SHA512.Create()
                    .ComputeHash(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(options))));
                var cache = GetQueryCache(hash);
                var startingPage = 1;
                int lastRecord;

                if (cache.TryGetValue(page - 1, out lastRecord))
                    startingPage = page;

                for (var i = startingPage; i <= page; i++)
                {
                    var results = GetNextPage(options, query, lastRecord, repoMethod);
                    cache.AddOrUpdate(i, results.LastRecord, (k, v) => results.LastRecord);
                    models = results.Logs;
                }
            }
            else
            {
                models = GetNextPage(options, query, (page - 1) * PAGE_SIZE, repoMethod).Logs;
            }
            
            models = models?
                .OrderByDescending(m => m.CreatedUTC)
                .ThenByDescending(m => m.LoggingSequenceNumber)
                .ToList();

            return models;
        }

        private ConcurrentDictionary<int, int> GetQueryCache(string queryHash)
        {
            if (_queryPageCount.TryGetValue(queryHash, out var queryCache))
                return queryCache;
            
            var newCache = new ConcurrentDictionary<int, int>();
            _queryPageCount.AddOrUpdate(queryHash, newCache, (k, v) => newCache);

            return newCache;
        }

        private PageResults GetNextPage(LogsQueryOptions options, string query, int lastRecord, Func<LogsQueryOptions, List<Log>> repoMethod)
        {
            var recordsThisPage = 0;
            var matches = new List<Models.Log>();
            var skipSearch = string.IsNullOrEmpty(query);

            while (matches.Count <= PAGE_SIZE)
            {
                options.Skip = recordsThisPage + lastRecord;

                var found = repoMethod(options).Select(_logsBuilder.Build).ToList();

                foreach (var log in found)
                {
                    if (skipSearch || log.MessageBody.IndexOf(query, StringComparison.OrdinalIgnoreCase) != -1)
                        matches.Add(log);

                    recordsThisPage += 1;
                    
                    if(matches.Count == PAGE_SIZE)
                        break;
                }

                if (found.Count < PAGE_SIZE || matches.Count == PAGE_SIZE) break;
            }

            return new PageResults(lastRecord + recordsThisPage, matches);
        }

        private Func<LogsQueryOptions, List<Log>> GetRepositoryMethod(List<string> severities)
        {
            if (severities.All(s => s.Equals("error", StringComparison.OrdinalIgnoreCase) || s.Equals("fatal", StringComparison.OrdinalIgnoreCase)))
                return (options) => _logsRepository.GetErrors(options);
            
            return (options) => _logsRepository.GetLogs(options);
        }

        private class PageResults
        {
            public int LastRecord { get; }
            public List<Models.Log> Logs { get; }

            public PageResults(int lastrecord, List<Models.Log> logs)
            {
                LastRecord = lastrecord;
                Logs = logs;
            }
        }
    }
}
