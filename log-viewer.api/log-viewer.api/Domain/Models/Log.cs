﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace logviewer.api.Domain.Models
{
    public class Log
    {
        public long EventMessageID { get; set; }
        public DateTime CreatedUTC { get; set; }
        public DateTime FileUTC { get; set; }
        public DateTime DatabaseUTC { get; set; }
        public int ThreadID { get; set; }
        public int LoggingSequenceNumber { get; set; }
        public string Severity { get; set; }
        public string ApplicationName { get; set; }
        public string MachineName { get; set; }
        public string LoggerName { get; set; }
        public string MessageBody { get; set; }
        public string UserName { get; set; }
        public string CurrentPrincipalName { get; set; }
        public string MachineIpAddress { get; set; }
        public string AssemblyName { get; set; }
        public string AssemblyVersion { get; set; }
        public string AssemblyCommitNumber { get; set; }
        public string CorrelationID { get; set; }
        public string ErrorCode { get; set; }
        public Guid IdentityId { get; set; }
    }
}
