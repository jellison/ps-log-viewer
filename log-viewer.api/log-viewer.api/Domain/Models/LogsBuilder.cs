﻿using AutoMapper;
using logviewer.api.Infrastructure;

namespace logviewer.api.Domain.Models
{
    public interface ILogsBuilder
    {
        Log Build(Data.Logs.Log entity);
    }

    public class LogsBuilder : ILogsBuilder
    {
        private static readonly IMapper _mapper;
        private readonly ICrypto _crypto;

        static LogsBuilder()
        {
            _mapper = new MapperConfiguration(ConfigureMapper).CreateMapper();
        }

        public LogsBuilder(ICrypto crypto)
        {
            _crypto = crypto;
        }

        public Log Build(Data.Logs.Log entity)
        {
            var model = _mapper.Map<Log>(entity);

            model.MessageBody = _crypto.Decrypt(entity.MessageBody);

            return model;
        }

        public static void ConfigureMapper(IMapperConfigurationExpression config)
        {
            config.CreateMap<Data.Logs.Log, Log>()
                .ForMember(m => m.MessageBody, o => o.Ignore());
        }
    }
}
