﻿using System;
using System.Collections.Generic;
using logviewer.api.Domain.Managers;
using Microsoft.AspNetCore.Mvc;

namespace logviewer.api.Http.Controllers
{
    [Route("[controller]")]
    public class LogsController : Controller
    {
        private readonly LogsManager _logsManager;

        public LogsController(LogsManager logsManager)
        {
            _logsManager = logsManager;
        }

        [HttpGet]
        public JsonResult Index([FromQuery] DateTime rangeStart, [FromQuery] DateTime rangeEnd, [FromQuery] string applicationName, [FromQuery] string query = null, [FromQuery] int page = 1, [FromQuery] List<string> severity = null)
        {
            return Json(_logsManager.GetLogs(applicationName, query, page, rangeStart, rangeEnd, severity));
        }
    }
}
