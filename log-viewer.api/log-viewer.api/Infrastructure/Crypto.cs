﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace logviewer.api.Infrastructure
{
    public interface ICrypto
    {
        string Decrypt(string cipherText);
    }

    public class Crypto : ICrypto
    {
        private readonly byte[] _key;
        private readonly byte[] _iv;

        public Crypto(string key, string iv)
        {
            _key = Convert.FromBase64String(key);
            _iv = Convert.FromBase64String(iv);
        }

        public string Decrypt(string cipherText)
        {
            var plainText = string.Empty;

            if (string.IsNullOrEmpty(cipherText.Trim()))
            {
                return plainText;
            }
            
            cipherText = cipherText.Replace("\r", "").Replace("\n", "").Trim();

            var algo = Aes.Create();

            try
            {
                using (var encryptorDecryptor = algo.CreateDecryptor(_key, _iv))
                {
                    using (var memStream = new MemoryStream(Convert.FromBase64String(cipherText)))
                    {
                        memStream.Position = 0;
                        using (var crStream = new CryptoStream(memStream, encryptorDecryptor, CryptoStreamMode.Read))
                        {
                            using (var strReader = new StreamReader(crStream))
                            {
                                plainText = strReader.ReadToEnd();
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                plainText = cipherText;
            }
            
            return plainText;
        }
    }
}
