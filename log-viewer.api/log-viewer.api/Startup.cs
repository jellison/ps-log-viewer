﻿using System;
using System.Diagnostics;
using logviewer.api.Domain.Data;
using logviewer.api.Domain.Managers;
using logviewer.api.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using StructureMap;
using Swashbuckle.AspNetCore.Swagger;

namespace logviewer.api
{
    public class Startup
    {
        public IConfigurationRoot Configuration { get; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json")
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().AddControllersAsServices();
            services.AddSwaggerGen(c => c.SwaggerDoc("v1", new Info { Title = "Log Viewer API", Version = "v1" }));

            return ConfigureIoC(services);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());
            app.UseMvc();
            
            if (!env.IsProduction())
            {
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Log Viewer API v1");
                });
            }
        }

        private IServiceProvider ConfigureIoC(IServiceCollection services)
        {
            var container = new Container();

            container.Configure(config =>
            {
                config.Scan(_ =>
                {
                    _.AssemblyContainingType(typeof(Startup));
                    _.WithDefaultConventions();
                });

                config.For<IConnectionFactory>().Use<ConnectionFactory>()
                    .Ctor<string>().Is(Configuration.GetConnectionString("DbConnectionString"));

                config.For<ICrypto>().Use<Crypto>()
                    .Ctor<string>("key").Is(Configuration.GetSection("Encryption")["CryptoKey"])
                    .Ctor<string>("iv").Is(Configuration.GetSection("Encryption")["CryptoVector"]);

                config.Populate(services);
            });

            Debug.WriteLine(container.WhatDidIScan());

            return container.GetInstance<IServiceProvider>();
        }
    }
}
