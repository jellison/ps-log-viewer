# LogViewer.Web

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.3.0.

## Getting Started
After cloning the repository, follow these steps to get up and running
1. Run `npm install`
2. Run `npm start`
3. There is no Step #3

## NPM Tasks
* Run `npm npm start` to build and run the development server.
* Run `npm run build` to build.
* Run `npm run test` to execute [Karma](https://karma-runner.github.io) tests.
* Run `npm run list` to execute Angular's list against source files.
