import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule }   from '@angular/forms';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { AppComponent } from './components/app.component';
import { LogsService } from './services/logs.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, FormsModule, InfiniteScrollModule
  ],
  providers: [LogsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
