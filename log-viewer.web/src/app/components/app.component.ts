import { Component, Inject, OnInit } from '@angular/core';
import { Observable } from 'rxjs/observable';
import * as Moment from 'moment';
import * as Toastr from 'toastr';

import { ListItem } from '../models/list-item';
import { Log } from '../models/log';
import { LogsService } from '../services/logs.service';
import { ToastMessage } from '../models/toast-message';

@Component({
    selector: 'app',
    templateUrl: '../templates/app.component.html',
    styleUrls: ['../styles/app.component.less']
})
export class AppComponent implements OnInit {
    private dateFormat: string = "MM/DD/YYYY HH:mm:ss";
    private page: number = 1;
    private pageAgain: boolean = false;

    logs: ListItem<Log>[];
    toast: ToastMessage = null;
    loading: boolean = false;

    applicationName: string = null;
    query: string = null;
    rangeStart: string;
    rangeEnd: string;
    filterDebug: string = "checked";
    filterInfo: string = "checked";
    filterWarning: string = "checked";
    filterError: string = "checked";
    filterFatal: string = "checked";    
    
    constructor(private logsService: LogsService){
    }

    /*
     * EVENT HANDLERS
     */
    ngOnInit(): void {        
        Toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "100",
            "hideDuration": "100",
            "timeOut": "0",
            "extendedTimeOut": "0",
            "showEasing": "linear",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        this.reset();
    }

    resetClicked(): void {
        this.reset();
    }

    goClicked(): void {        
        this.page = 1;
        this.getLogs().subscribe(logs => this.logs = logs);
    }

    rowClicked(log: ListItem<Log>): void {
        log.isSelected = !log.isSelected;
    }

    nextPage(): void{
        if(this.loading){
            this.pageAgain = true;
            return;
        }

        this.loading = true;
        this.getLogs(this.page + 1)
            .subscribe(
                logs => {
                    logs.map(log => this.logs.push(log));
                    this.page += 1;
                    this.loading = false;
                },
                null,
                () => {
                    if(this.pageAgain) {
                        this.pageAgain = false;
                        this.nextPage();
                    }
                }
            );
    }

    /*
     * PRIVATE METHODS
     */
    private reset(): void {
        this.page = 1;
        this.applicationName = null;
        this.query = null;
        this.toast = null;
        this.rangeEnd = Moment.utc().format(this.dateFormat);
        this.rangeStart = Moment.utc().subtract(30, 'minutes').format(this.dateFormat);
        this.filterDebug = 'checked';
        this.filterInfo = 'checked';
        this.filterWarning = 'checked';
        this.filterError = 'checked';
        this.filterFatal = 'checked';
        
        this.getLogs().subscribe(logs => this.logs = logs);
    }

    private getLogs(page: number = null): Observable<ListItem<Log>[]> {
        let severities: string[] = [];

        if(this.filterDebug) severities.push('Debug');
        if(this.filterInfo) severities.push('Info');
        if(this.filterWarning) severities.push('Warning');
        if(this.filterError) severities.push('Error');
        if(this.filterFatal) severities.push('Fatal');

        Toastr.info('Loading ...');
        this.loading = true;

        let observable = this.logsService.getLogs(
                Moment(this.rangeStart, this.dateFormat),
                Moment(this.rangeEnd, this.dateFormat),
                this.applicationName,
                this.query,
                page,
                severities)
            .map(logs => logs.map(item => new ListItem<Log>(item)));

        observable.subscribe(null,
            (err) => this.handleError(err),
            () =>{ Toastr.clear(); this.loading = false; });

        return observable;
    }

    private handleError(error: any){
        this.toast = { message: error.message, severity: "danger"};
    }
}
