export class ListItem<T> {
    data: T;
    isSelected: boolean;

    constructor(data: T){
        this.data = data;
    }
}