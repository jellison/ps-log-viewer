import * as Moment from 'moment';

export class Log {
    // IDs
    createdUtc: Moment.Moment;
    sequence: number;
    correlationId: string;

    // Metadata
    machineName: string;
    applicationName: string;
    message: string;
    severity: string;
}