export class ToastMessage {
    message: string;
    severity: string;
}