import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/observable';
import 'rxjs/add/operator/map';

import * as Moment from 'moment';
import { Log } from '../models/log';
import { UrlBuilder } from '../utils/url-builder';

@Injectable()
export class LogsService {
    constructor(private httpClient: HttpClient)
    {        
    }

    getLogs(rangeStart: Moment.Moment, rangeEnd: Moment.Moment, applicationName: string = null, query: string = null, page: number = null, severities: string[] = null): Observable<Log[]> {
        var builder = new UrlBuilder("http://localhost:4300/logs");

        builder.addParameter('rangeStart', rangeStart.format());
        builder.addParameter('rangeEnd', rangeEnd.format());
        builder.addParameter('applicationName', applicationName);        
        builder.addParameter('query', query);        
        builder.addParameter('page', page);
        builder.addParameter('severity', severities);

        var ret = this.httpClient.get<LogDto[]>(builder.build()).map(res => {
            let ret : Log[] = [];

            for(let log of res){
                ret.push({
                    createdUtc: Moment(log.createdUTC),
                    sequence: log.loggingSequenceNumber,
                    correlationId: log.correlationID,
                    message: log.messageBody,
                    machineName: log.machineName,
                    applicationName: log.applicationName,
                    severity: log.severity
                });
            }

            return ret;
        });

        return ret;
    }
}

class LogDto {
    // IDs
    createdUTC: string;
    loggingSequenceNumber: number;
    correlationID: string;

    // Metadata
    machineName: string;
    applicationName: string;
    messageBody: string;
    severity: string;
}