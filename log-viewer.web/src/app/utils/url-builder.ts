export class UrlBuilder {    
    private parameters: string[] = [];

    constructor(private baseUrl: string){
    }

    addParameter(key: string, value: any){
        if(value){
            if(Array.isArray(value)){
                value.map(v => this.parameters.push(`${key}=${v}`));
            } else {
                this.parameters.push(`${key}=${value}`);
            }            
        }        
    }

    build(): string {
        let url: string = this.baseUrl;

        if(this.parameters.length > 0){
            url += '?';
            url += this.parameters.join('&');
        }

        return url;
    }
}